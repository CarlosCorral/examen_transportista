<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TransportistaController;
use App\Http\Controllers\InicioController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[InicioController::class,'inicio']);
Route::get('/transportistas',[TransportistaController::class,'mostrarTransportistas'])->name('transportistas.index');
Route::get('/transportistas/entregar',[TransportistaController::class,'entregar'])->name('transportistas.entregar');
Route::get('/transportistas/noentregado',[TransportistaController::class,'mostrarTransportista'])->name('transportistas.noentregar');
Route::get('/transportistas/{transportista}',[TransportistaController::class,'mostrarTransportista'])->name('transportistas.show');
Route::get('/transportistas/crear',[PaquetesController::class,'crearPaquete'])->name('paquetes.crear');
Route::post('/transportistas/crear',[PaquetesController::class,'create'])->name('paquetes.create');

