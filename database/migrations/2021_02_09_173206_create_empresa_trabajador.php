<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresaTrabajador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_trabajador', function (Blueprint $table) {
             $table->unsignedBigInteger('transportista_id');
            $table->unsignedBigInteger('empresa_id');
            $table->foreign('transportista_id')->references("id")->on("transportistas");
            $table->foreign('empresa_id')->references("id")->on("empresas");
            $table->primary(['transportista_id','empresa_id']); 
            $table->timestamps();
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_trabajador');
    }
}
