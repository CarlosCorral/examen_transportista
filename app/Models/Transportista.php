<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Empresa;
use App\Models\Paquete;
class transportista extends Model
{
    use HasFactory;
    protected  $table="transportistas";
    protected $guarded=[];

    public function paquetes()
    {
    return $this->hasMany(Paquete::class);
    }

    public function empresas()
    {
    return $this->belongsToMany(Empresa::class);
    }
}
