<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Transportista;
class empresa extends Model
{
    use HasFactory;
    protected  $table="empresas";
    protected $guarded=[];

    public function transportistas()
    {
    return $this->belongsToMany(Transportista::class);
    }
}
