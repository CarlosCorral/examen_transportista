<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Transportista;
class paquete extends Model
{
    use HasFactory;
    protected  $table="paquetes";
    protected $guarded=[];

    public function transportista(){
        return $this->belongsTo(Transportista::class);
    }
}
