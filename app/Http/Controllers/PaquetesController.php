<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaquetesController extends Controller
{
    public function crearPaquete(){
        return view('paquetes.create');
    }
    public function create(Request $request){
        $datos["direccion"]= $request->direccion;
        $datos["transportista_id"]=$request->transportista->id;
        $datos['imagen']=$request->imagen->store('','paquetes');
        $paquete=Paquete::create($paquete);
        return redirect()->route("paquetes.create");
    }
}
