<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Paquete;
use App\Models\Transportista;
class TransportistaController extends Controller
{
   public function mostrarTransportistas(){
        $transportistas=Transportista::all();
        return view('transportistas.index',["transportistas"=>$transportistas]);
   }

   public function mostrarTransportista(Transportista $transportista){
        return view('transportistas.show',["transportista"=>$transportista]);
   }

   public function entregar(Transportista $transportista){
    $mensaje=null;   
    if(count($transportista->paquetes)>0){
           $paquetes= Paquete::all()->where('id_transportista',$transportista->id);
           foreach($paquete as $paquete){
               $paquete->entregado=true;
               $paquete->save();
               $mensaje="Estan todos entregados";
           }
       }
    return view('transportistas.show',["transportista"=>$transportista,"mensaje"=>$mensaje]);
}

public function noEntregar(Transportista $transportista){
    $mensaje=null;   
    if(count($transportista->paquetes)>0){
           $paquetes= Paquete::all()->where('id_transportista',$transportista->id);
           foreach($paquete as $paquete){
               $paquete->entregado=false;
               $paquete->save();
               $mensaje="Estan todos como no entregados";
           }
       }
    return view('transportistas.show',["transportista"=>$transportista,"mensaje"=>$mensaje]);
}
}
