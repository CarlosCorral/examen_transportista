<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\TransportistaController;
class InicioController extends Controller
{
    public function inicio(){
        return redirect()->action([TransportistaController::class,'mostrarTransportistas']);
    }
}
